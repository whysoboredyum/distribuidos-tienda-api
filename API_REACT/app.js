const express = require("express");
const mysql = require('mysql');

const app = express();

app.set('port',process.env.PORT || 9000)
//crear coneccion

const db = mysql.createConnection({
    host: "localhost",
    user: "root",
    password: "",
    database: "mydb",
});

//conectarse a la base de datos
db.connect((err)=>{
    if(err){
        throw err;
    }
    console.log("Coneccion hecha")
});

//Select posts
app.get("/getrepuestos", (req, res) => {
    let sql = "SELECT * FROM repuestos";
    let query = db.query(sql, (err, result) => {
      if (err) throw err;
      console.log(result);
      res.send(result);
    });
  });

app.listen(app.get('port'),() =>{
    console.log(`Servidor corriendo en puerto`,app.get('port'))
});
import React from "react";
import { Header } from "./componentes/Header";
import 'boxicons';
import {BrowserRouter as Router} from "react-router-dom";
import {Paginas} from "./componentes/paginas"
import { Carrito } from "./componentes/Carrito";
import { DataProvider } from "./context/Dataprovider"


function App() {
  return (
    <div className="App">
      <DataProvider>
      <Router>
        <Header />
        <Carrito/>
        <Paginas />
      </Router>
      </DataProvider>
    </div>
  );
}

export default App;

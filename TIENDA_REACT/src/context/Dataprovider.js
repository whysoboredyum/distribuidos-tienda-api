import React, { createContext, useEffect, useState } from "react";
export const DataContext = createContext();

export const DataProvider = (props) =>{
    const [productos, setProductos] = useState([])
    const [menu, setMenu] =useState(false);
    const [carrito,setCarrito] = useState([])

    useEffect(() => {
        const getRepuestos = () =>{
            fetch('http://localhost:9000/getrepuestos')
            .then(res => res.json())
            .then(res => setProductos(res))
        }
        getRepuestos()
    },[])
    
    const addCarrito = (id) => {
        const check = carrito.every(item =>{
            return item.id !== id;
        })
        if(check){
            const data = productos.filter(producto =>{
                return producto.id === id
            })
            setCarrito([...carrito,...data])
        }else{
            alert("El producto se ha añadido al carrito")
        }
    }

    useEffect(()=>{
        const dataCarrito = JSON.parse(localStorage.getItem('carrito'))
        if(dataCarrito){
            setCarrito(dataCarrito)
        }
    },[])

    useEffect(() =>{
        localStorage.setItem('dataCarrito',JSON.stringify(carrito))
    },[carrito])

    const value = {
        productos : [productos],
        menu : [menu,setMenu],
        addCarrito: addCarrito,
        carrito: [carrito,setCarrito]
    }

    return (
        <DataContext.Provider value={value}>
            {props.children}
        </DataContext.Provider>
    )
}